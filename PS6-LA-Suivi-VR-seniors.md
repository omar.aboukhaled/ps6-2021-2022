---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Suivi des patients affectés de démence pendant la thérapie en réalité virtuelle
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Quentin Meteier
  - Leonardo Angelini
mots-clés: [eye-tracking, Unity, realité virtuelle]
langue: [F,E]
confidentialité: oui
suite: non
---

```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth]{img/HP-ReverbG2.png}
\end{center}
```

## Description/Contexte

De plus en plus de casques de réalité virtuelle (RV) sont proposés aux établissements médico-sociaux (EMS) afin de mener des expériences ludiques et apaisantes, grâce à des vidéos immersives à 360°, permettant d’explorer des environnements naturels ou touristiques stimulants. Cet outil a suscité la curiosité des seniors et de plusieurs professionnels, mais a aussi levé plusieurs questionnements sur les bienfaits et sur l’utilisabilité pour des personnes affectées de démence. Le but de ce projet est de concevoir et développer un outil qui puisse aider le personnel des EMS à mener des expériences thérapeutiques avec des résidents. Cet outil permettrait de mieux comprendre les ressentis des seniors qui expérimentent des vidéos immersives ou des expériences interactives en RV, ressentis qui sont souvent cachés par le casque lui-même et que les seniors avec des situations de démence plus grave ne pourront pas exprimer verbalement. Techniquement, grâce aux nouveaux casques avec suivi du regard et capteurs psychophysiologiques intégrés (HP Reverb G2 Omincept), il s’agira de montrer en temps réel au personnel soignant la position du regard du senior sur la vidéo 360° ainsi que l’état psychophysiologique du senior, grâce à l’analyse du rythme cardiaque, de la dilatation des pupilles et des expressions faciales. Cela permettra aux animateurs de mieux guider le senior lors de l’expérience et, en particulier, de vérifier les réactions du senior à leurs inputs (p.ex. : « regardez ce papillon sur la droite ! »). Dans un deuxième temps, des expériences interactives (p. ex., un papillon animé qui se déplace dès qu’on le regarde) pourraient être réalisées.

## Objectifs

- Analyse du casque et de l’SDK fourni par HP
- Analyse des technologies existantes pour la réalisation de l'inteface de suivi
- Conception du système et de l’interface pour le soignant
- Récolte de données avec des personnes en bonne santé
- Analyse de donnés pour vérifier la qualité des signaux physiologiques collectés par le casque
- Implémentation de l’interface soignant 
- Tests du système avec des personnes en bonne santé et un soignant

## Contraintes

- Utilisation du casque HP Reverb G2 Omnicept

