---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: PMF-Vision - Tracking d'objets en temps-réél
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Simon Ruffieux
mots-clé: [Image Processing, Image Registration, Object Tracking, Assembly Station]
langue: [F,E]
confidentialité: oui
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth]{img/Apercu_TabledeMontage.PNG}
\end{center}
```
## Contexte

L'entreprise PMF-System SA basée à Marly produit des meubles spécialement pensés pour les entreprises. Elle produit notamment des tables de montage (Assembly Workspace ou Assembly Table) pour diverses PME locales et cherche à rendre ces tables intelligentes. Dans le cadre d'un projet Innosuisse, l'institut HumanTech développe un prototype fonctionnel de table de montage. Cette table de montage intelligente est composée de caméras, d'un projecteur et de lampes (tous installés au-dessus de la table et pointant vers le bas (top-view)).
L'utilisation de la caméra (Kinect Azure) permet de suivre les actions effectuées par l'opérateur (suivi de la position des mains, prise d'une pièce, dépose d'une pièce) afin de garantir que toutes les étapes de l'assemblage sont réalisées dans le bon ordre. Le projecteur est utilisé pour fournir des informations à l'utilisateur directement sur l'espace de travail et pour mettre en valeur certaines parties du montage en cours de réalisation. Les différents calculs sont réalisés sur un ordinateur embarqué de type Jetson AGX Xavier (Linux).  

A terme, ce système a pour but d'aider les opérateurs réaliser leurs tâches quotidiennes ainsi que de les assister lors de l'apprentissage de nouveaux scénarios de montage. Le projet a pour objectif de déployer ces tables dans différents ateliers-protégés partenaires.

Dans le cadre de ce projet PS6, l'étudiant sera responsable de développer un module Python permettant de suivre en temps-réel le décalage (x, y et rotation) entre la pièce réelle posée au centre de la table et son image de référence sotckées en mémoire. Ces informations (x,y,r) nous permettront de corriger l'affichage des informations en réalité augmentée et ainsi garantir qu'elles soient correctement superposées à la pièce présente sur la table. Pour réaliser cette tâche, différents algorithmes ou combinaisons d'algorithmes sont potentiellement utilisables (SIFT, SURF, Edge detection, etc). Ce concept est généralement nommé "Image registration". L'étudiant sera responsable d'analyser les possibles candidats, d'implémenter la solution la plus adaptée et finalement de l'évaluer au travers de tests quantitatifs.


## Objectifs

- Analyse du système existant
- Recherche et analyse d'algorithmes adaptés au problème
- Conception
- Implémentation du prototype (hors du système existant)
- Tests quantitatifs

## Contraintes

- Python
