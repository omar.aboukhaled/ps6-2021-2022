---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Application d'autosuivi pour les patients affectés par la Sarcoïdose
filières:
  - Télécommunications
nombre d'étudiants: 2
professeurs co-superviseurs:
  - Leonardo Angelini
proposé par étudiant: Luc Parret
mots-clés: [auto-suivi, sarcoïdose, flutter]
langue: [F,E]
confidentialité: oui
suite: oui
---

## Description/Contexte

Dans le cadre du projet de semestre 5, en collaboration avec des étudiants de la Haute Ecole de Santé, une application pour l'autosoin de la sarcoïdose et été imaginée et conçue. Un premier prototype basique a été dévéloppé en Flutter. Le but de ce projet est d'implementer une première version de cette application qui puisse être testée avec des utilisateurs cible.


## Objectifs

- Analyse des retours utilisateurs obtenus lors des tests des maquettes
- Analyse des librairies pour la gestion du calendrier er pour la création de graphiques
- Analyse du marché et des couts liées au deployment de l'application
- Conception et implementation de l'application
- Tests utilisateur

## Contraintes

- Continuation de l'app developpée en Flutter
