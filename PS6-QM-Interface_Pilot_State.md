---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Interface de visualisation de l'état physiologique d'un pilote de chasse
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Quentin Meteier
proposé par étudiant: Jacquat Jonathan
mots-clés: [pilot, interface, physiological state]
langue: [F,E]
confidentialité: oui
suite: non
---


## Description/Contexte

LD Switzerland SA est une société suisse spécialisée dans la conception, la fabrication et la commercialisation de casques aéronautiques, de masques à oxygène pour pilotes et de systèmes de surveillance de la santé des pilotes. Elle a été fondée pour créer des produits innovants en termes de design, de fonctionnalité et de sécurité en mettant en œuvre de nombreux dispositifs/fonctionnalités brevetés. En particulier, l'entreprise entreprend de grandes innovations pour développer des systèmes permettant d'analyser l'état du pilote pendant et après le vol. Dans ce contexte, un premier prototype de capteurs physiologiques intégré dans le casque du pilote a été développé. Ces capteurs permettent de mesurer différents signaux et indicateurs physiologiques tels que la variabilité cardiaque (Heart Rate Variability en anglais) ou la saturation en oxygène dans le sang (Sp02). L'institut HumanTech doit améliorer le protoype existant pour le rendre exploitable et permettre de réellement mesurer l'état du pilote à partir des signaux captés. De plus, le système devra ensuite être plus intelligent pour reconnnaître automatiquement certains évènements.

Une première pipeline automatisant le traitement du signal PPG et le calcul d'indicateurs de variabilité cardiaque a déjà été développée en Python. Le but de ce projet de semestre est de faire une interface web permettant de visualiser les signaux du pilote et afficher les indicateurs physiologiques sur une période donnée. Des informations liées au profil du pilote pourront également être affichées.
Dans un 2ème temps, le but sera d'améliorer la pipeline de traitement du signal en détectant les endroits où la qualité du signal est mauvaise à cause des mouvements du pilote ( ou motion artefacts en anglais), et corriger/traiter cette partie du signal avec l'aide d'un accéléromètre. Les parties où le signal est de mauvaise qualité pourront également être mises en évidence sur l'interface. 


## Objectifs

- Analyse de la pipeline existante
- Analyse des technologies à utiliser pour le front-end (interface), le back-end et la sauvegarde des données
- Analyse des techniques permettant de mesurer la qualité du signal PPG et de correction d'artefacts avec un acccéléromètre
- Conception de l'architecture du système
- Implémentation de la vue
- Implémentation de l'algorithme de calcul de la qualité du signal et de correction des artefacts
- Tests fonctionnels avec quelques utilisateurs et comparaison avec valeur de référence
- Optionnel : Ajout d'intelligence dans le système avec reconnaissance d'évènements

## Contraintes

- Pipeline existante (Python) permettant de traiter les signaux et calculer les indicateurs physiologiques
