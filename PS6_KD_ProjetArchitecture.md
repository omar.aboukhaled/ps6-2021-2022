---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Platforme web pour les architectes - Three.js
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Karl Daher
mandants: 
  - Fish Studio - Grégory Schafer
mots-clés: [architecture, development]
langue: [F,E]
confidentialité: non
suite: oui
---

## Description/Contexte

Le projet d’architecture vise à construire une plateforme web (Laravel, vue js), permettant aux architectes de présenter aux partenaires et aux clients, leurs modèles 3D sans qu’ils aient besoin de ressources techniques (hardware) importantes. La plateforme sera construite pour permettre aux utilisateurs d’intégrer leurs modèles 3D et, basée sur la librairie Three JS, de les laisser s’immerger dans le projet. Les architectes pourront sur cette plateforme, déposer leurs fichiers – avec ou sans texture – et retoucher les différents points techniques nécessaires à l’immersion. Ces points de contrôles seront les suivants :
-	Ajout ou suppression de texture
-	Choix des limites des visites, de l’endroit d’apparition et de déplacement
-	Interaction des 3D (raycasting)
-	Interaction en VR et AR

## Objectifs

Cette plateforme web leur permettra :
-	De charger les 3D et de pouvoir les partager à d’autres membres (Iframes, code embede ou lien)
-	De modifier certaines textures et de choisir le positionnement de l’immeuble face au soleil
-	De choisir quelles interactions pourront être effectuées sur les 3D par les clients ou d’autres membres du projet
-	D’annoter des commentaires et de pouvoir les partager


## Refrences

- https://showroom.littleworkshop.fr/
- https://biganto.com/gallery/21273/?sort=created
- https://biganto.com/gallery/21031/?sort=created

