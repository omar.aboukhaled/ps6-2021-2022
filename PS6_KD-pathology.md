---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Plateforme d’analyse d’image pour détection des tissus sains et non sains
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Cyril Berton
  - Karl Daher
mandants: 
  - Département de médecine/pathologie UNIFR - Dr. Jimmy Stalin et Prof. Curzio Ruegg
mots-clés: [image anylsis, Color Analysis, Anomaly Detection]
langue: [F,E]
confidentialité: oui
suite: oui
---

## Description/Contexte

Dans le cadre d’une étude en pathologie médicale, nous souhaitons améliorer une plateforme existante sur laquelle nous pourrions traiter et analyser des images des tissus pulmoniares. 
Le projet se concentre sur la détection des cellules ou tissus sain et pas-sains. Nous nous concentrons sur la détection de l’altération pulmonaire dans la fibrose et la détection de différents types de cellules dans les poumons. 
Le tissue est normalement coloré et scanné par un microscope. 
Type de coloration : 
-	Coloration rouge pour les tissus pas sains 
-	Coloration bleu violet pour les tissus pas sains
-	Coloration brune est utilisée pour la détection de cellules spécifiques  
Les images sont transférées par le scanner sous l’extension ‘.ndpi’, les détails du software sont sous ce lien : 
https://nanozoomer.hamamatsu.com/jp/en/product/search/U12388-01/index.html 

Nous avons besoin de définir 1/ les sections d’intérêt (alvéole pulmonaire) puis de définir 2/ s'il s’agit de tissus sains ou pas-sains et 3/ la présence et la quantification de cellules d’intérêt pour le suivi de l’évolution de la maladie. 
Les résultats seront analysés en parallèle par un expert de recherche medical afin de valider le processus.


## Objectifs

- Analyse du dataset fourni et prise en main du code existant
- Migrer la platforme sur un nouveau framework
- Amélioration de l'algorithme de détection  
- Test et validation 

